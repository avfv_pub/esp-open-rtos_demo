#include "reefcloud_system.h"
#include "reefcloud_rtos.h"

namespace std
{

void __throw_length_error(char const*)
{
	reefcloud::rtos::global_fault();
}

}
