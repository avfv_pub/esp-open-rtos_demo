#pragma once

#include <stdarg.h>
#include <string.h>
#include <new>
#include <utility>
#include "make_unique.h"

#include "espressif/esp_common.h"
#include "esp/uart.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/dns.h"
#include "lwip/api.h"

#include "dhcpserver.h"

namespace reefcloud { namespace data {

typedef uint16_t size_t;

}}
