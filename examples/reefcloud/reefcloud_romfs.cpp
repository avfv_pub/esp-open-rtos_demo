#include "reefcloud_romfs.h"

#include <espressif/spi_flash.h>
#include <stdbool.h>
//#include "esp_spiffs_flash.h"
#include "flashchip.h"
#include "esp/rom.h"
#include "esp/spi_regs.h"

/**
 * Copy 4-byte aligned source data to unaligned destination buffer.
 *
 * @param bytes Number of byte to copy to dst.
 *
 * @see unaligned_memcpy.S
 */
extern "C"
{
    void memcpy_unaligned_dst(uint8_t *dst, volatile uint32_t *src, uint8_t bytes);
}

#define ESP_SPIFFS_FLASH_OK        0
#define ESP_SPIFFS_FLASH_ERROR     1
#define SPIFFS_ERR_INTERNAL        -10050
#define SPIFFS_OK                  0

namespace reefcloud { namespace romfs {

// 64 bytes read causes hang
// http://bbs.espressif.com/viewtopic.php?f=6&t=2439
#define SPI_READ_MAX_SIZE   60

/**
 * Read SPI flash up to 64 bytes.
 */
static inline void IRAM read_block(sdk_flashchip_t *chip, uint32_t addr,
        uint8_t *buf, uint32_t size)
{
    SPI(0).ADDR = (addr & 0x00FFFFFF) | (size << 24);
    SPI(0).CMD = SPI_CMD_READ;

    while (SPI(0).CMD) {};

    memcpy_unaligned_dst(buf, SPI(0).W, size);
}

/**
 * Read SPI flash data. Data region doesn't need to be page aligned.
 */
static inline uint32_t IRAM read_data(sdk_flashchip_t *flashchip, uint32_t addr,
        uint8_t *dst, uint32_t size)
{
    if (size < 1) {
        return ESP_SPIFFS_FLASH_OK;
    }

    if ((addr + size) > flashchip->chip_size) {
        return ESP_SPIFFS_FLASH_ERROR;
    }

    while (size >= SPI_READ_MAX_SIZE) {
        read_block(flashchip, addr, dst, SPI_READ_MAX_SIZE);
        dst += SPI_READ_MAX_SIZE;
        size -= SPI_READ_MAX_SIZE;
        addr += SPI_READ_MAX_SIZE;
    }

    if (size > 0) {
        read_block(flashchip, addr, dst, size);
    }

    return ESP_SPIFFS_FLASH_OK;
}

uint32_t IRAM esp_spiffs_flash_read(uint32_t dest_addr, uint8_t *buf, uint32_t size)
{
    uint32_t result = ESP_SPIFFS_FLASH_ERROR;

    if (buf) {
        vPortEnterCritical();
        Cache_Read_Disable();

        result = read_data(&sdk_flashchip, dest_addr, buf, size);

        Cache_Read_Enable(0, 0, 1);
        vPortExitCritical();
    }

    return result;
}

static s32_t esp_spiffs_read(u32_t addr, u32_t size, u8_t *dst)
{
    if (esp_spiffs_flash_read(addr, dst, size) == ESP_SPIFFS_FLASH_ERROR) {
        return SPIFFS_ERR_INTERNAL;
    }

    return SPIFFS_OK;
}

filesystem::filesystem(uint32_t base_addr):
    m_base_addr(base_addr)
{
}

filesystem::~filesystem()
{
}

errors filesystem::mount()
{
    if (m_base_addr == 0) return errors::mount_failed;
    
    printf("Mount romfs at %u... ", m_base_addr);

    m_root = find_root_file();

    if (m_root.is_empty())
    {
        m_base_addr = 0;
        return errors::mount_failed;
    }

    printf("ok\r\n");

    auto current_file = m_root;
    while (!current_file.is_empty())
    {
        char buf[128];
        current_file.get_name(buf, 128);

        printf("file: %s\r\n", buf);

        current_file = current_file.next(*this);
    }

    return errors::ok;
}

file filesystem::find(const char* path) const
{
    file current_file = m_root;
    
    if (path[0] == '/')
    {
        path++;
    }

    printf("find file: %s\r\n", path);

    bool last_part = false;

    while (!last_part)
    {
        uint32_t part_size = 0;
        auto slash_pos = strchr(path, '/');
        if (slash_pos)
        {
            part_size = slash_pos - path;
        }
        else
        {
            part_size = strlen(path);
            last_part = true;
        }

        printf("part:%s %d", path, part_size);

        if (part_size == 0)
        {
            return file(0);
        }

        while (!current_file.is_empty())
        {
            char buf[256];
            current_file.get_name(buf, 256);

            printf("probe: %s\r\n", buf);

            if (strncmp(path, buf, part_size) == 0)
            {
                if (strlen(buf) == part_size)
                {
                    goto part_found;
                }
            }

            current_file = current_file.next(*this);
        }

        return file(0);

part_found: {}
        printf("found!\r\n");

        if (current_file.is_folder())
        {
            current_file = current_file.sub(*this);

            if (last_part) return file(0);
        }

        path += part_size;
        path++;
    }


    return current_file;
}

file filesystem::find_root_file() const
{
    file root_file(0);

    {
        uint8_t buf[8];

        if (esp_spiffs_read(m_base_addr, 8, buf) != SPIFFS_OK)
        {
            printf("failed: flash error\r\n");
            return root_file;
        }

        if (strncmp(reinterpret_cast<const char*>(buf), "-rom1fs-", 8) != 0)
        {
            printf("failed: -rom1fs- signature not found\r\n");
            return root_file;
        }
    }

    {
        uint8_t buf[17];
        memset(buf, 0, 17);

        auto volume_addr = m_base_addr + 16;

        printf("found volume:");

        while (true)
        {
            if (esp_spiffs_read(volume_addr, 16, buf) != SPIFFS_OK)
            {
                printf("failed: flash error\r\n");
                return root_file;
            }

            volume_addr += 16;

            printf("%s", buf);

            if (strlen(reinterpret_cast<const char*>(buf)) < 16)
            {
                break;
            }
        }

        printf(" ");

        root_file = file(volume_addr);
    }

    return root_file;
}


file::file():
    m_base_addr(0)
{
}

file::file(uint32_t base_addr):
    m_base_addr(base_addr)
{

}

file::file(const file& x):
    m_base_addr(x.m_base_addr)
{

}

file::~file()
{
    m_base_addr = 0;
}

file& file::operator=(const file& x)
{
    m_base_addr = x.m_base_addr;
    
    return *this;
}

bool file::is_empty() const
{
    return m_base_addr == 0;
}

file file::next(const filesystem& fs) const
{
    file next_file(0);
    
    uint32_t buf;

    if (esp_spiffs_read(m_base_addr, 4, reinterpret_cast<uint8_t*>(&buf)) != SPIFFS_OK)
    {
        return next_file;
    }
    
    auto next_offset = (ntohl(buf) & 0xfffffff0);
    if (next_offset != 0)
    {
        next_file = file(fs.m_base_addr + next_offset);
    }

    return next_file;
}

bool file::is_folder() const
{
    uint32_t buf;

    if (esp_spiffs_read(m_base_addr, 4, reinterpret_cast<uint8_t*>(&buf)) != SPIFFS_OK)
    {
        return false;
    }
    
    auto file_type = ntohl(buf) & 0x07;

    return file_type == 1;
}

file file::sub(const filesystem& fs) const
{
    uint32_t buf;

    if (esp_spiffs_read(m_base_addr + 4, 4, reinterpret_cast<uint8_t*>(&buf)) != SPIFFS_OK)
    {
        return file(0);
    }
    
    return file(fs.m_base_addr + (ntohl(buf) & 0xfffffff0));
}

void file::get_name(char* buffer, uint32_t buffer_size) const
{
    if (is_empty())
    {
        *buffer = 0;
        return;
    }
    
    uint8_t buf[17];
    memset(buf, 0, 17);

    auto name_addr = m_base_addr + 16;

    uint32_t readed_size = 0;
    buffer_size--;
    if (buffer_size == 0)
    {
        *buffer = 0;
        return;
    }

    while (true)
    {
        if (esp_spiffs_read(name_addr, 16, buf) != SPIFFS_OK)
        {
            *buffer = 0;
            return;
        }

        name_addr += 16;

        auto part_size = strlen(reinterpret_cast<const char*>(buf));
        
        if ((readed_size + part_size) > buffer_size)
        {
            part_size = buffer_size - readed_size;
        }

        readed_size += part_size;

        if (part_size > 0)
        {
            memcpy(buffer, buf, part_size);
            buffer += part_size;
        }

        if (part_size < 16)
        {
            break;
        }
    }

    *buffer = 0;
}

uint32_t file::file_size() const
{
    if (is_empty())
    {
        return 0;
    }

    uint32_t buf;

    if (esp_spiffs_read(m_base_addr + 8, 4, reinterpret_cast<uint8_t*>(&buf)) != SPIFFS_OK)
    {
        return 0;
    }
    
    return ntohl(buf);
}

uint32_t file::data_position() const
{
    uint8_t buf[17];
    memset(buf, 0, 17);

    auto name_addr = m_base_addr + 16;

    while (true)
    {
        if (esp_spiffs_read(name_addr, 16, buf) != SPIFFS_OK)
        {
            return 0;
        }

        name_addr += 16;

        auto part_size = strlen(reinterpret_cast<const char*>(buf));
        
        if (part_size < 16)
        {
            break;
        }
    }

    return name_addr;
}

void file::read(uint32_t& position, uint8_t* buffer, uint32_t size) const
{
    esp_spiffs_read(position, size, buffer);
    position += size;
}

}}
