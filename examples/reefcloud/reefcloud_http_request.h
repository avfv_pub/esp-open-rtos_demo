#pragma once
#include "reefcloud_system.h"
#include "reefcloud_rtos_queue.h"
#include <vector>

namespace reefcloud { namespace http {

const size_t MAX_BODY_SIZE = 1024;
const size_t MAX_HEADER_LINE_SIZE = 128;

class request
{
public:
	request();

	enum states : uint8_t
	{
		receiving_header,
		receiving_body,
		received_ok,
		bad_request
	};

	enum methods : uint8_t
	{
		get,
		post,
		unknown
	};

	void receive(netconn* connection);

	states state() const;
	methods method() const;
	const char* host() const;
	const char* uri() const;
	std::vector<uint8_t>& body();

	void clear_body();

private:
	states m_state = states::bad_request;
	methods m_method = methods::unknown;
	std::vector<char> m_host;
	std::vector<char> m_uri;

	std::vector<char> m_header_line;

	size_t m_body_size = 0;
	std::vector<uint8_t> m_body;

	void process_rx(char new_char);

	const char* is_header(const char* header) const;
	bool get_header(const char* header, std::vector<char>& data) const;
};

}}
