#pragma once
#include "reefcloud_system.h"
#include "reefcloud_rtos.h"

namespace reefcloud { namespace rtos {

#pragma pack(push, 1)
template<typename T, data::size_t MAX_SIZE>
class queue
{
public:
	queue()
	{
		m_handle = xQueueCreate(MAX_SIZE, sizeof(T));
	}

	~queue()
	{
		vQueueDelete(m_handle);
	}

	void reset()
	{
		xQueueReset(m_handle);
	}

	void put(const T& value)
	{
		while (xQueueSend(m_handle, &value, 1000) != pdPASS)
		{
			rtos::wait(1);
		}
	}

	bool put(const T& value, uint32_t wait)
	{
		return xQueueSend(m_handle, &value, wait) == pdPASS;
	}

	T get()
	{
		T value;

		while (xQueueReceive(m_handle, &value, 1000) != pdPASS)
		{
			rtos::wait(1);
		}

		return value;
	}

	bool get(T& value, uint32_t wait)
	{
		return xQueueReceive(m_handle, &value, wait) == pdPASS;
	}

private:
	xQueueHandle m_handle = nullptr;
};
#pragma pack(pop)

}}
