#include "reefcloud_rtos.h"

namespace reefcloud { namespace rtos {

void start_task(task_function_t task_func, void * const parameter, task_handle_t * const task)
{
	if (xTaskCreate(task_func, (const char *)"task", 2048, parameter, 2, task) != pdPASS)

	//if (xTaskCreate(task_func, nullptr, 1024*4, parameter, tskIDLE_PRIORITY, task) != pdPASS)
	{
		global_fault();
	}
}

task_handle_t start_task(task_function_t task_func, void * const parameter, data::size_t stack_size)
{
	task_handle_t task;

	if (xTaskCreate(task_func, (const char *)"task", stack_size, parameter, 2, &task) != pdPASS)
	{
		global_fault();
	}

	return task;
}

void terminate_current_task()
{
	vTaskDelete(nullptr);
	while (true);
}

void wait(uint32_t milliseconds)
{
	portTickType ticks = milliseconds / portTICK_RATE_MS;
	vTaskDelay(ticks ? ticks : 1);
}

void global_fault()
{
	taskDISABLE_INTERRUPTS();
	while (true);
}

void global_assert_ptr(const void* ptr, const char* error)
{
	if (ptr) return;

	printf("global_assert: %s", error);

	taskDISABLE_INTERRUPTS();
	while (true);
}

void global_assert_zero(uint32_t value, const char* error)
{
	if (value == 0) return;

	printf("global_assert: %s %d", error, value);

	taskDISABLE_INTERRUPTS();
	while (true);
}

void global_assert_nonzero(uint32_t value, const char* error)
{
	if (value != 0) return;

	printf("global_assert: %s", error);

	taskDISABLE_INTERRUPTS();
	while (true);
}

}}
