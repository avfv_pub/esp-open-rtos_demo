#pragma once
#include "reefcloud_system.h"
#include "reefcloud_rtos.h"

namespace reefcloud { namespace dns {

enum class opcodes
{
	QUERY = 0,
	IQUERY = 1,
	STATUS = 2
};

enum class response_codes
{
	no_error = 0,
	form_error = 1,
	server_failure = 2,
	non_existent_domain = 3,
	not_implemented = 4,
	refused = 5,
	YX_domain = 6,
	YXRR_set = 7,
	NXRR_set = 8
};

#pragma pack(push, 1)
struct packet_header
{
	uint16_t ID;
	uint8_t RD : 1;
	uint8_t TC : 1;
	uint8_t AA : 1;
	uint8_t Opcode : 4;
	uint8_t QR : 1;
	uint8_t RCODE : 4;
	uint8_t Z : 3;
	uint8_t RA : 1;
	uint16_t QDCOUNT;
	uint16_t ANCOUNT;
	uint16_t NSCOUNT;
	uint16_t ARCOUNT;
};
#pragma pack(pop)

class server
{
public:
	server(const char* dns_names, ip_addr_t ip);
	~server();

	void start();

private:
	const char* m_dns_names = nullptr;
	ip_addr_t m_ip;

	rtos::task_handle_t m_udp_server_task = nullptr;
	netconn* m_udp_netconn = nullptr;

	static const data::size_t m_udp_buffer_size = 128;
	uint8_t m_udp_buffer[m_udp_buffer_size];
	data::size_t m_packet_size = 0;

	static const data::size_t m_domain_buffer_size = 128;
	char m_domain_buffer[m_domain_buffer_size];
	uint16_t QTYPE = 0;
	uint16_t QCLASS = 0;

	bool m_is_resolved = false;

	void udp_server_task();
	void parse_question_section();
	void make_udp_response();
	void make_udp_non_existent_domain_response();
	void make_udp_not_implemented_response();
};

}}
