#include "reefcloud_dns_server.h"

namespace reefcloud { namespace dns {

//#define DEBUG_REEFCLOUD_DNS

server::server(const char* dns_names, ip_addr_t ip):
	m_dns_names(dns_names),
	m_ip(ip)
{

}

server::~server()
{
}

void server::start()
{
	m_udp_server_task = rtos::start_task([](void* parameter)
	{
		reinterpret_cast<server*>(parameter)->udp_server_task();
	}, this, 1024);
}

void server::udp_server_task()
{
	m_udp_netconn = netconn_new(NETCONN_UDP);
	rtos::global_assert_ptr(m_udp_netconn, "failed to allocate DNS UDP socket");

	err_t last_error;

	last_error = netconn_bind(m_udp_netconn, IP_ADDR_ANY, 53);
	rtos::global_assert_zero(last_error, "DNS UDP socket netconn_bind error");

    while(true)
    {
        netbuf *netbuf;

        last_error = netconn_recv(m_udp_netconn, &netbuf);
        if(last_error != ERR_OK)
        {
            continue;
        }

	    auto from_addr = netbuf_fromaddr(netbuf);
	    auto from_port = netbuf_fromport(netbuf);
	    m_packet_size = netbuf_len(netbuf);

//	    printf("udp %d.%d.%d.%d:%d\n",
//	    	ip4_addr1(from_addr), ip4_addr2(from_addr),
//			ip4_addr3(from_addr), ip4_addr4(from_addr),
//			from_port);

	    memset(m_udp_buffer, 0, m_udp_buffer_size);

        netbuf_copy(netbuf, m_udp_buffer, m_udp_buffer_size);
        netbuf_delete(netbuf);

        m_is_resolved = false;

        make_udp_response();

        if (m_packet_size == 0)
        {
        	continue;
        }

		netbuf = netbuf_new();
		netbuf_alloc(netbuf, m_packet_size);
		netbuf_take(netbuf, m_udp_buffer, m_packet_size);
		netconn_sendto(m_udp_netconn, netbuf, from_addr, from_port);
		netbuf_delete(netbuf);
    }
}

void server::make_udp_response()
{
	auto header = reinterpret_cast<packet_header*>(m_udp_buffer);

	header->AA = 1;
	header->RA = 0;

#ifdef DEBUG_REEFCLOUD_DNS
	printf("DNS QR:%d Opcode:%d QD:%d AN:%d NS:%d AR:%d\n",
		header->QR, header->Opcode,
		htons(header->QDCOUNT),
		htons(header->ANCOUNT),
		htons(header->NSCOUNT),
		htons(header->ARCOUNT));
#endif		

	if (header->QR != 0)
	{
		make_udp_not_implemented_response();
		return;
	}

	if (static_cast<opcodes>(header->Opcode) != opcodes::QUERY)
	{
		make_udp_not_implemented_response();
		return;
	}

	if (ntohs(header->QDCOUNT) != 1)
	{
		make_udp_not_implemented_response();
		return;
	}

	if (header->ANCOUNT != 0)
	{
		make_udp_not_implemented_response();
		return;
	}

	if (header->NSCOUNT != 0)
	{
		make_udp_not_implemented_response();
		return;
	}

	if (header->ARCOUNT != 0)
	{
		make_udp_not_implemented_response();
		return;
	}

	parse_question_section();

	auto domain_name = static_cast<char*>(m_domain_buffer);

#ifdef DEBUG_REEFCLOUD_DNS	
	printf("Question:%s QTYPE:%d QCLASS:%d\n", domain_name, QTYPE, QCLASS);
#endif	

	if (QTYPE != 1)
	{
		make_udp_not_implemented_response();
		return;
	}

	m_is_resolved = true;

	if (!strstr(m_dns_names, reinterpret_cast<const char*>(domain_name)))
	{
		m_is_resolved = false;
		make_udp_non_existent_domain_response();
		return;
	}
	else
	{
#ifdef DEBUG_REEFCLOUD_DNS
		printf("DNS RESOLVED!\n");
#endif		
	}

	header->QR = 1;
	header->ANCOUNT = htons(1);
	header->RCODE = static_cast<uint8_t>(response_codes::no_error);

	auto response = reinterpret_cast<uint8_t*>(m_udp_buffer) + m_packet_size;
	auto name_size = strlen(domain_name) + 1 + 2 + 2;

	memcpy(response, reinterpret_cast<const uint8_t*>(m_udp_buffer) + sizeof(packet_header), name_size);
	response += name_size;

	*response = 0;
	response++;
	*response = 0;
	response++;
	*response = 60;
	response++;
	*response = 60;
	response++;

	*response = 0;
	response++;

	*response = 4;
	response++;

	if (m_is_resolved)
	{
		memcpy(response, &m_ip, 4);
		response += 4;
	}
	else
	{
	    ip_addr_t fake_ip;
	    IP4_ADDR(&fake_ip, 127, 0, 0, 1);

		memcpy(response, &fake_ip, 4);
		response += 4;
	}

	m_packet_size = response - reinterpret_cast<uint8_t*>(m_udp_buffer);
}

void server::parse_question_section()
{
	QTYPE = 0xffff;
	QCLASS = 0xffff;

	auto question = reinterpret_cast<const uint8_t*>(m_udp_buffer) + sizeof(packet_header);

	auto domain_name = static_cast<char*>(m_domain_buffer);
	auto pos = domain_name;
	while (true)
	{
		auto label_size = *question;
		question++;

		if (label_size == 0 || label_size > 63)
		{
			*pos = 0;
			break;
		}

		*pos = '.';
		pos++;

		if (((pos - domain_name) + 1) >= m_domain_buffer_size)
		{
			*pos = 0;
			return;
		}

		for (uint8_t i = 0; i < label_size; i++)
		{
			*pos = *question;
			pos++;
			question++;

			if (((pos - domain_name) + 1) >= m_domain_buffer_size)
			{
				*pos = 0;
				return;
			}
		}
	}

	memcpy(&QTYPE, question, 2);
	QTYPE = ntohs(QTYPE);
	question += 2;

	memcpy(&QCLASS, question, 2);
	QCLASS = ntohs(QCLASS);
	question += 2;
}

void server::make_udp_non_existent_domain_response()
{
#ifdef DEBUG_REEFCLOUD_DNS
	printf("make_udp_non_existent_domain_response\n");
#endif

	auto header = reinterpret_cast<packet_header*>(m_udp_buffer);
	header->QR = 1;
	header->RCODE = static_cast<uint8_t>(response_codes::non_existent_domain);
}

void server::make_udp_not_implemented_response()
{
#ifdef DEBUG_REEFCLOUD_DNS
	printf("make_udp_not_implemented_response\n");
#endif

	auto header = reinterpret_cast<packet_header*>(m_udp_buffer);
	header->QR = 1;
	header->RCODE = static_cast<uint8_t>(response_codes::not_implemented);
}

}}
