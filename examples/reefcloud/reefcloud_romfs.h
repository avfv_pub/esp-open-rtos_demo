#pragma once
#include "reefcloud_system.h"
#include "reefcloud_rtos.h"

namespace reefcloud { namespace romfs {

enum class errors
{
    ok = 0,
    mount_failed = 1,
    not_found = 2
};

class filesystem;

class file
{
public:
    file();
    file(uint32_t base_addr);
    file(const file& x);
    ~file();

    file& operator=(const file& x);

    bool is_empty() const;    
    file next(const filesystem& fs) const;
    bool is_folder() const;
    file sub(const filesystem& fs) const;
    void get_name(char* buffer, uint32_t buffer_size) const;
    uint32_t file_size() const;
    uint32_t data_position() const;
    void read(uint32_t& position, uint8_t* buffer, uint32_t size) const;

private:
    uint32_t m_base_addr = 0;
};

class filesystem
{
friend file;

public:
	filesystem(uint32_t base_addr);
	~filesystem();

	errors mount();

    file find(const char* path) const;

private:
    uint32_t m_base_addr = 0;
    file m_root;

    file find_root_file() const;
};

}}
