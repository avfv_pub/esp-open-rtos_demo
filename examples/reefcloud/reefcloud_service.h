#pragma once
#include "reefcloud_system.h"
#include "reefcloud_rtos.h"
#include "reefcloud_romfs.h"
//#include "reefcloud_spiffs.h"

namespace reefcloud {

class service
{
public:
	service();

	void start();

	uint32_t m_test = 0;

	rtos::task_handle_t m_web_server_task = nullptr;
	void web_server_task();

	void setup_access_point();
	void start_web_server();
private:
	std::unique_ptr<romfs::filesystem> m_admin_files;	

	//std::unique_ptr<spiffs::filesystem> m_admin_files;

//	void process_web_request(http::request& rq);
//	void send_not_found(http::request& rq);
//	bool make_not_found(http::request& rq);

	void process_static_get(netconn* conn, const char* uri);
	void send_not_found(netconn* conn);
	void send_http_response_header(netconn* conn, int32_t content_size, const char* content_type);
	const char* get_mime_type(const char* uri) const;
};

}
