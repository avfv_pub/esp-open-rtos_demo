#pragma once
#include "reefcloud_system.h"
#include "reefcloud_rtos.h"

namespace reefcloud { namespace json {

enum class positions
{
    object_start,
    object_end,
    key,
    value,
    value_end,
    array_end,
    error
};

class inplace_editor;

typedef bool (*on_pos_function_t)( inplace_editor );

class inplace_editor
{
public:
    inplace_editor(char* buffer, uint32_t buffer_size);
    inplace_editor(const inplace_editor& x);
    ~inplace_editor();

    inplace_editor& operator=(const inplace_editor& x);

    bool is_obj_start() const;
    bool is_obj_end() const;
    bool is_key() const;
    bool is_value() const;
    bool is_error() const;

    void clear();
    void seek_begin();

    void enum_keys(on_pos_function_t on_key);

    inplace_editor get_key(const char* key) const;
    inplace_editor get_path(const char* key) const;

private:
    char* m_buffer = nullptr;
    uint32_t m_size = 0;
    char* m_buffer_pos = nullptr;
    positions m_pos = positions::object_start;

    bool eof() const;
    char ch() const;
    void skip_spaces();
    void skip_char();

    void skip_key();
    void skip_value();
    void skip_object();
    void skip_number();
    void skip_array();
    void skip_null();
    void skip_string();
};

}}
