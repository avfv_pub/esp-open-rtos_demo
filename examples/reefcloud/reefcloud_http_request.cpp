#include "reefcloud_http_request.h"

namespace reefcloud { namespace http {

#define DEBUG_HTTP_RECEIVE

request::request()
{
}

void request::receive(netconn* connection)
{
	m_state = states::receiving_header;
	m_header_line.clear();
	m_header_line.reserve(128);
	m_host.clear();
	m_uri.clear();
	m_method = methods::unknown;
	m_body_size = 0;
	clear_body();

	netbuf* received_buffer = nullptr;

    while (true)
    {
#ifdef DEBUG_HTTP_RECEIVE
    	printf("netconn_recv\n");
#endif

    	if (netconn_recv(connection, &received_buffer) != ERR_OK)
		{
#ifdef DEBUG_HTTP_RECEIVE
    	printf("netconn_recv fail\n");
#endif
    		return;
		}

		while (true)
		{
			char* received_data = nullptr;
			uint16_t received_size = 0;

#ifdef DEBUG_HTTP_RECEIVE
			printf("netbuf_data\n");
#endif

			netbuf_data(received_buffer, reinterpret_cast<void**>(&received_data), &received_size);

#ifdef DEBUG_HTTP_RECEIVE
			printf("netbuf_data rec %d\n", received_size);
#endif

			for (uint16_t i = 0; i < received_size; i++)
			{
#ifdef DEBUG_HTTP_RECEIVE
				printf("%c", received_data[i]);
#endif
				process_rx(received_data[i]);
			}

#ifdef DEBUG_HTTP_RECEIVE
			printf("netbuf_next\n");
#endif

			if (netbuf_next(received_buffer) != ERR_OK)
			{
#ifdef DEBUG_HTTP_RECEIVE
			printf("netbuf_next fail\n");
#endif

				break;
			}
		}

#ifdef DEBUG_HTTP_RECEIVE
		printf("netbuf_delete\n");
#endif

		netbuf_delete(received_buffer);

#ifdef DEBUG_HTTP_RECEIVE
		printf("state %d\n", m_state);
#endif

		if (m_state == states::received_ok || m_state == states::bad_request)
		{
			return;
		}
    }
}

request::states request::state() const
{
	return m_state;
}

request::methods request::method() const
{
	return m_method;
}

const char* request::host() const
{
	return m_host.data();
}

const char* request::uri() const
{
	return m_uri.data();
}

std::vector<uint8_t>& request::body()
{
	return m_body;
}

void request::clear_body()
{
	decltype(m_body)().swap(m_body);
}

void request::process_rx(char new_char)
{
	if (m_state == states::receiving_body)
	{
		m_body.push_back(new_char);
		if (m_body.size() >= m_body_size)
		{
			m_state = states::received_ok;
			return;
		}
	}
	else if (m_state == states::receiving_header)
	{
		m_header_line.push_back(new_char);

		if (m_header_line.size() < 2)
		{
			return;
		}

		auto header_line_tail = m_header_line.end() - 2;

		if (*header_line_tail != '\r')
		{
			return;
		}
		header_line_tail++;
		if (*header_line_tail != '\n')
		{
			return;
		}

		if (m_header_line.size() == 2)
		{
			m_host.push_back(0);
			m_uri.push_back(0);

			if (m_body_size > MAX_BODY_SIZE)
			{
				m_state = states::bad_request;
				return;
			}

			if (m_body_size == 0)
			{
				m_state = states::received_ok;
				return;
			}

			m_body.reserve(m_body_size);
			m_state = states::receiving_body;
			return;
		}

		auto pos = is_header("Content-Length:");
		if (pos)
		{
			m_body_size = 0;
			while (true)
			{
				auto c = *pos;
				pos++;

				if (c == ' ') continue;
				if (c < '0' || c > '9') break;

				m_body_size *= 10;
				m_body_size += c - '0';
			}
			goto drop_header_line;
		}

		if (get_header("GET", m_uri))
		{
			m_method = methods::get;
			goto drop_header_line;
		}
		if (get_header("POST", m_uri))
		{
			m_method = methods::post;
			goto drop_header_line;
		}
		if (get_header("Host:", m_host))
		{
			goto drop_header_line;
		}

		drop_header_line: {}
		m_header_line.clear();
	}
}

const char* request::is_header(const char* header) const
{
	auto pos = m_header_line.data();
	while (*header != 0)
	{
		if (*pos != *header)
		{
			return nullptr;
		}
		pos++;
		header++;
	}
	return pos;
}

bool request::get_header(const char* header, std::vector<char>& data) const
{
	auto pos = is_header(header);
	if (!pos)
	{
		return false;
	}

	data.clear();

	while (true)
	{
		if (*pos != ' ') break;

		pos++;
	}

	while (true)
	{
		auto c = *pos;
		pos++;

		if (c == ' ') break;
		if (c == '\r') break;

		data.push_back(c);
	}
	return true;
}


}}
