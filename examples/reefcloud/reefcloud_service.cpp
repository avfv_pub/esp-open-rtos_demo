#include "reefcloud_service.h"
#include "reefcloud_rtos.h"
#include "reefcloud_http_request.h"

#include "lwip/tcp.h"
#include "lwip/api_msg.h"

namespace reefcloud {

#define DEBUG_SERVICE

service::service()
{
	m_test = 12345678;
}

void service::start()
{
#ifdef DEBUG_SERVICE
	printf("service::start %p\n", this);
    printf("service::m_test %d\n", m_test);
	printf("Free heap %d bytes\n", (int)xPortGetFreeHeapSize());
#endif
	setup_access_point();

    m_web_server_task = rtos::start_task([](void* argument)
	{
#ifdef DEBUG_SERVICE
	    printf("service::start 1\n");
		printf("Free heap %d bytes\n", (int)xPortGetFreeHeapSize());
#endif

		reinterpret_cast<service*>(argument)->web_server_task();
	}, this, 1512);
}

void service::web_server_task()
{	
	//rtos::wait(5000);
	//setup_access_point();
	start_web_server();

//    rtos::start_task([](void* argument)
//	{
//		reinterpret_cast<service*>(argument)->start_web_server();
//	}, this, 256);

	int prev_heap = 0;

	while (true)
	{
		auto cur_heap = (int)xPortGetFreeHeapSize();
	    //printf("web\n");
		if (cur_heap != prev_heap)
		{
#ifdef DEBUG_SERVICE
			printf("Free heap %d bytes\n", cur_heap);
#endif
			prev_heap = cur_heap;
		}

		rtos::wait(1000);
	}
}

void service::setup_access_point()
{
#ifdef DEBUG_SERVICE
    printf("setup_access_point\n");
#endif

    printf("setup_access_point ...\n");

	//sdk_wifi_set_opmode_current(SOFTAP_MODE);
	sdk_wifi_set_opmode_current(STATIONAP_MODE);

	struct sdk_station_config station_config;
	strcpy(reinterpret_cast<char*>(station_config.ssid), "SSID");
	strcpy(reinterpret_cast<char*>(station_config.password), "pwdpwd");
	sdk_wifi_station_set_config(&station_config);


	//sdk_wifi_set_opmode(SOFTAP_MODE);
    struct ip_info ap_ip;
    IP4_ADDR(&ap_ip.ip, 192, 168, 4, 1);
    IP4_ADDR(&ap_ip.gw, 192, 168, 4, 1);
    IP4_ADDR(&ap_ip.netmask, 255, 255, 255, 0);
    sdk_wifi_set_ip_info(1, &ap_ip);

    auto SSID = "REEFCLOUD";
    auto SSID_PWD = "rfaaabbbcccddd";

#ifdef DEBUG_SERVICE
    printf("setup_access_point 1\n");
#endif

    sdk_softap_config ap_config;
    memset(reinterpret_cast<char*>(ap_config.ssid), 0, 32);
	strcpy(reinterpret_cast<char*>(ap_config.ssid), SSID);
    ap_config.ssid_len = 0;
    ap_config.ssid_hidden = 0;
	ap_config.channel = 5;
	ap_config.authmode = AUTH_WPA_WPA2_PSK;
    memset(reinterpret_cast<char*>(ap_config.password), 0, 64);
	strcpy(reinterpret_cast<char*>(ap_config.password), SSID_PWD);
	ap_config.max_connection = 4;
	ap_config.beacon_interval = 100;
    sdk_wifi_softap_set_config_current(&ap_config);
    //sdk_wifi_softap_set_config(&ap_config);

#ifdef DEBUG_SERVICE
    printf("setup_access_point 2\n");
#endif

    //printf((char*)&ap_config.ssid[0]);
    //printf((char*)&ap_config.password[0]);

    ip_addr_t first_client_ip;
    IP4_ADDR(&first_client_ip, 192, 168, 4, 10);
    dhcpserver_start(const_cast<ip_addr_t const*>(&first_client_ip), (uint8_t)4);

#ifdef DEBUG_SERVICE
    printf("setup_access_point 3\n");
#endif

}

void service::start_web_server()
{
#ifdef DEBUG_SERVICE
    printf("start_web_server\n");
#endif

	m_admin_files = std::make_unique<romfs::filesystem>(0x200000);
	m_admin_files->mount();

    err_t last_error = ERR_OK;

	auto nc = netconn_new(NETCONN_TCP);
	if (!nc)
	{
#ifdef DEBUG_SERVICE
		printf("Status monitor: Failed to allocate socket.\r\n");
#endif
		return;
	}

#ifdef DEBUG_SERVICE
    printf("netconn_new ok\n");
#endif

    last_error = netconn_bind(nc, IP_ADDR_ANY, 80);
	if (last_error != ERR_OK)
	{
#ifdef DEBUG_SERVICE
	    printf("netconn_bind err %d\n", last_error);
#endif
	    return;
	}

	//last_error = netconn_listen(nc);
	last_error = netconn_listen_with_backlog(nc, 0);
	if (last_error != ERR_OK)
	{
#ifdef DEBUG_SERVICE
	    printf("netconn_listen err %d\n", last_error);
#endif
	    return;
	}

    netconn* client = nullptr;

	while (true)
	{
#ifdef DEBUG_SERVICE
	    printf("netconn_accept\n");
#endif
	    if (netconn_accept(nc, &client) != ERR_OK)
	    {
#ifdef DEBUG_SERVICE
		    printf("netconn_accept fail\n");
#endif

	    	client = nullptr;
	    	continue;
	    }

		netconn_set_recvtimeout(client, 5000);
		tcp_nagle_disable(client);

#ifdef DEBUG_SERVICE
	    ip_addr_t client_addr;
	    uint16_t port_ignore;
	    netconn_peer(client, &client_addr, &port_ignore);

	    printf("new connection %u\n", client_addr);
#endif


	    auto rq = std::make_unique<http::request>();

		auto q = rq.get();

		http::request* qqq = nullptr;



#ifdef DEBUG_SERVICE
	    printf("rq allocated\n");
#endif

	    rq->receive(client);

#ifdef DEBUG_SERVICE
	    printf("packet received\n");
#endif

	    if (rq->state() == http::request::states::received_ok)
	    {
#ifdef DEBUG_SERVICE
	    	printf("Receive ok, method:%d host:%s uri:%s\n",
	    			rq->method(),
					rq->host(),
					rq->uri());
#endif

			if (rq->method() == http::request::methods::get)
			{
				process_static_get(client, rq->uri());
			}
			else
			{
				char buf[80];

				snprintf(buf, sizeof(buf), "Heap %d bytes\n", (int)xPortGetFreeHeapSize());
				netconn_write(client, buf, strlen(buf), NETCONN_COPY);
			}

	    }
	    else
	    {
#ifdef DEBUG_SERVICE
	    	printf("Receive bad\n");
#endif

	    	// char buf[80];

			// snprintf(buf, sizeof(buf), "Error! Heap %d bytes\n", (int)xPortGetFreeHeapSize());
		    // netconn_write(client, buf, strlen(buf), NETCONN_COPY);
	    }

#ifdef DEBUG_SERVICE
	    printf("cleanup 1\n");
#endif

	    rq.reset();

#ifdef DEBUG_SERVICE
	    printf("cleanup 2\n");
#endif

		if (client)
    	{
			//rtos::wait(5000);
			netconn_shutdown(client, NETCONN_SHUT_RD, NETCONN_SHUT_WR);
			//netconn_close(client);
			netconn_delete(client);
    		client = nullptr;
    	}

#ifdef DEBUG_SERVICE
	    printf("cleanup 3\n");
#endif
	}
}

void service::process_static_get(netconn* conn, const char* uri)
{
	const char* routed_uri = uri;
	
	if (strcmp(uri, "/") == 0)
	{
		routed_uri = "/index.html";
	}
	
	auto file = m_admin_files->find(routed_uri);
	if (file.is_empty())
	{
		send_not_found(conn);
		return;
	}

	char buf[128];
	file.get_name(buf, 128);

	printf("found file:%s\r\n", buf);

	auto file_size = file.file_size();
	if (file_size <= 0)
	{
		send_not_found(conn);
		return;
	}

	printf("SEND FILE %d\r\n", file_size);

	send_http_response_header(conn, file_size, get_mime_type(routed_uri));

	printf("MIME:%s\r\n", get_mime_type(routed_uri));

	auto reading_position = file.data_position();

	while (file_size > 0)
	{
		printf("FS:%d HEAP:%u\r\n", file_size, (int)xPortGetFreeHeapSize());
		//printf("RPOS:%d\r\n", reading_position);
		//printf("HEAP:%u\r\n", (int)xPortGetFreeHeapSize());
		
		const uint32_t buf_size = 1460;//512;

		uint8_t buf[buf_size];

		auto to_read = file_size;
		if (to_read > buf_size) to_read = buf_size;

		//printf("TRD:%d\r\n", to_read);

		if (to_read > 0)
		{
			file.read(reading_position, buf, to_read);

			//printf("RR_POS:%d\r\n", reading_position);

			//for (int i = 0; i < 100; i++) printf("%c", buf[i]);
			//printf("\r\n");

			auto err = netconn_write(conn, buf, to_read, NETCONN_NOCOPY);

			//printf("ERR: %d\r\n", err);

			//auto err1 = tcp_output(conn->pcb.tcp);
			//printf("tcp_output err %d\r\n", err1);
			
		}

		file_size -= to_read;
	}

	printf("SENDED\r\n");
}

void service::send_not_found(netconn* conn)
{
	auto msg = "404 not found";
	auto msg_size = strlen(msg);
	send_http_response_header(conn, msg_size, "text/plain");
	netconn_write(conn, msg, msg_size, NETCONN_COPY);
}

void service::send_http_response_header(netconn* conn, int32_t content_size, const char* content_type)
{
	char buf[150];

	snprintf(buf, sizeof(buf), "HTTP/1.1 200 OK\r\nContent-Length: %d\r\nContent-Type: %s; charset=UTF-8\r\n\r\n", content_size, content_type);

	auto err = netconn_write(conn, buf, strlen(buf), NETCONN_COPY);
	printf("send_http_response_header err%d\r\n", err);

	//auto err1 = tcp_output(conn->pcb.tcp);
	//printf("tcp_output err %d\r\n", err1);
}

const char* service::get_mime_type(const char* uri) const
{
	auto dot = strrchr(uri, '.');
	if (!dot)
	{
		return "application/octet-stream";
	}

	if (!strcmp(dot, ".jpg"))
	{
		return "image/jpg";
	}
	else if (!strcmp(dot, ".ico"))
	{
		return "image/ico";
	}
	else if (!strcmp(dot, ".png"))
	{
		return "image/png";
	}
	else if (!strcmp(dot, ".html"))
	{
		return "text/html";
	}
	else if (!strcmp(dot, ".css"))
	{
		return "text/css";
	}
	else if (!strcmp(dot, ".js"))
	{
		return "text/javascript";
	}
	
	return "application/octet-stream";
}

}
