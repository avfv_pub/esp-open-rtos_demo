#include "reefcloud_json.h"

namespace reefcloud { namespace json {

inplace_editor::inplace_editor(char* buffer, uint32_t buffer_size):
    m_buffer(buffer),
    m_size(buffer_size),
    m_buffer_pos(buffer),
    m_pos(positions::object_start)
{

}

inplace_editor::inplace_editor(const inplace_editor& x):
    m_buffer(x.m_buffer),
    m_size(x.m_size),
    m_buffer_pos(x.m_buffer_pos),
    m_pos(x.m_pos)
{

}

inplace_editor::~inplace_editor()
{

}

inplace_editor& inplace_editor::operator=(const inplace_editor& x)
{
    m_buffer = x.m_buffer;
    m_size = x.m_size;
    m_buffer_pos = x.m_buffer_pos;
    m_pos = x.m_pos;

    return *this;
}

void inplace_editor::clear()
{
    m_buffer[0] = '{';
    m_buffer[1] = '}';
    m_buffer[2] = 0;

    seek_begin();
}

bool inplace_editor::is_obj_start() const
{
    return m_pos == positions::object_start;
}

bool inplace_editor::is_obj_end() const
{
    return m_pos == positions::object_end;
}

bool inplace_editor::is_key() const
{
    return m_pos == positions::key;
}

bool inplace_editor::is_value() const
{
    return m_pos == positions::value;
}

bool inplace_editor::is_error() const
{
    return m_pos == positions::error;
}


void inplace_editor::seek_begin()
{
    m_buffer_pos = m_buffer;
    m_pos = positions::object_start;

    skip_spaces();
}

void inplace_editor::enum_keys(on_pos_function_t on_key)
{
    if (m_pos != positions::object_start || eof())
    {
        return;
    }

    skip_char();

    while (true)
    {
        skip_spaces();
        if (eof())
        {
            return;
        }
        if (ch() == '}')
        {
            m_pos = positions::object_end;
            return;
        }

        m_pos = positions::key;

        inplace_editor current_pos(*this);

        if (!on_key(current_pos))
        {
            return;
        }

        skip_key();
        skip_value();
        skip_spaces();

        if (eof())
        {
            return;
        }

        if (ch() == ',')
        {
            skip_char();
        }
    }
    
}

inplace_editor inplace_editor::get_key(const char* key) const
{
    auto current_pos = *this;
    auto found_pos = *this;
        
    current_pos.enum_keys([&](inplace_editor pos)
    {
        pos.skip_spaces();
        if (pos.eof()) return false;
        pos.skip_char();

        auto k = key;

        while (!pos.eof())
        {
            auto jc = pos.ch();
            auto kc = *k;
            
            if (jc == '"')
            {
                if (kc == '/' || kc == 0)
                {
                    found_pos = current_pos;
                    return false;
                }
            }

            if (jc != kc)
            {
                return true;
            }

            pos.skip_char();
            k++;
        }

        return true;
    });

    if (found_pos == positions::key)
    {
        return found_pos;
    }

    return current_pos;
}

inplace_editor inplace_editor::get_path(const char* key) const
{
    auto current_pos = *this;

    while (true)
    {
        current_pos = current_pos.get_key(key);
        if (current_pos.is_key())
        {
            
        }

    }
}

bool inplace_editor::eof() const
{
    if (m_pos == positions::error) return true;

    return *m_buffer_pos == 0 || (uint32_t)(m_buffer_pos - m_buffer) >= m_size;
}

char inplace_editor::ch() const
{
    return *m_buffer_pos;
}

void inplace_editor::skip_spaces()
{
    while (true)
    {
        if (eof()) return;
        if (ch() != ' ') return;

        skip_char();
    }
}

void inplace_editor::skip_char()
{
    m_buffer_pos++;
}

void inplace_editor::skip_key()
{
    while (!eof())
    {
        auto c = ch();
        if (c == '"')
        {
            skip_char();
            break;
        }
        else if (c == ' ')
        {
            skip_char();
        }
        else
        {
            m_pos = positions::error;
            return;
        }
    }

    while (!eof())
    {
        auto c = ch();
        if (c == '"')
        {
            skip_char();
            break;
        }
        skip_char();
    }
    
    while (!eof())
    {
        auto c = ch();
        if (c == ':')
        {
            skip_char();
            break;
        }
        else if (c == ' ')
        {
            skip_char();
        }
        else
        {
            m_pos = positions::error;
            return;
        }
    }

    m_pos = positions::value;
}

void inplace_editor::skip_value()
{
    skip_spaces();
    if (eof()) return;

    auto c = ch();
    if (c == '{')
    {
        m_pos = positions::object_start;
        skip_object();
        return;
    }
    else if (c == '-' || (c >= '0' && c <= '9') || c == '.')
    {
        skip_number();
        return;
    }
    else if (c == '"')
    {
        skip_string();
        return;
    }
    else if (c == '[')
    {
        skip_array();
        return;
    }
    else if (c == 'n')
    {
        skip_null();
        return;
    }
    else
    {
        m_pos = positions::error;
    }
}

void inplace_editor::skip_object()
{
    enum_keys([](inplace_editor pos)
    {
        return true;
    });
    
    if (m_pos == positions::object_end)
    {
        skip_char();
        m_pos = positions::value_end;
    }
}

void inplace_editor::skip_number()
{
    while (!eof())
    {
        auto c = ch();
        if (c == '-' || (c >= '0' && c <= '9') || c == '.')
        {
            skip_char();
            continue;
        }
        else if (c == ',' || c == ']' || c == '}')
        {
            m_pos = positions::value_end;
            return;
        }
        else
        {
            m_pos = positions::error;
            return;
        }
    }
}

void inplace_editor::skip_array()
{
    skip_char();
    while (true)
    {
        skip_value();
        skip_spaces();
        if (eof()) return;

        auto c = ch();
        if (c == ']')
        {
            skip_char();
            m_pos = positions::value_end;
            return;
        }
        else if (c == ',')
        {
            skip_char();
        }
        else
        {
            m_pos = positions::error;
            return;
        }
    }
}

void inplace_editor::skip_null()
{
    if (eof()) return;
    if (ch() != 'n')
    {
        m_pos = positions::error;
        return;
    }
    skip_char();

    if (eof()) return;
    if (ch() != 'u')
    {
        m_pos = positions::error;
        return;
    }
    skip_char();

    if (eof()) return;
    if (ch() != 'l')
    {
        m_pos = positions::error;
        return;
    }
    skip_char();

    if (eof()) return;
    if (ch() != 'l')
    {
        m_pos = positions::error;
        return;
    }
    skip_char();

    m_pos = positions::value_end;
}

void inplace_editor::skip_string()
{
    skip_char();
    while (!eof())
    {
        auto c = ch();
        if (c == '"')
        {
            skip_char();
            m_pos = positions::value_end;
            return;
        }
        else if (c == '\\')
        {
            skip_char();
            if (eof()) return;
            skip_char();
        }
    }
}

}}