#pragma once
#include "reefcloud_system.h"
#include <new>

namespace reefcloud { namespace data {

#pragma pack(push, 1)
template<typename T>
class static_obj
{
public:
	T* operator->()
	{
		return reinterpret_cast<T*>(&m_placeholder);
	}

	template<typename... args>
	void initialize(args... params)
	{
		new (&m_placeholder) T(params...);
	}

private:
	char m_placeholder[sizeof(T)];
};
#pragma pack(pop)

}}
