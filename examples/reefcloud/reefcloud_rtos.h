#pragma once
#include "reefcloud_system.h"

#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"

namespace reefcloud { namespace rtos {

typedef void (*task_function_t)( void * );
typedef void * task_handle_t;

void start_task(task_function_t task_func, void * const parameter, task_handle_t * const task);
task_handle_t start_task(task_function_t task_func, void * const parameter, data::size_t stack_size);

void terminate_current_task();

void wait(uint32_t milliseconds);

void global_fault();

void global_assert_ptr(const void* ptr, const char* error);
void global_assert_zero(uint32_t value, const char* error);
void global_assert_nonzero(uint32_t value, const char* error);

}}
