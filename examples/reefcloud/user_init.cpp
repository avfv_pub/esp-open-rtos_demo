#include "reefcloud_system.h"
#include "reefcloud_data_static_obj.h"
#include "reefcloud_service.h"
#include "reefcloud_dns_server.h"
#include "reefcloud_rtos.h"
#include <vector>

reefcloud::data::static_obj<reefcloud::service> srv;
reefcloud::data::static_obj<reefcloud::dns::server> dns_srv;

extern "C" void sdk_hostap_handle_timer(void *cnx_node)
{
	//printf("sdk_hostap_handle_timer");
}

extern "C" void user_init(void)
{
	uart_set_baud(0, 115200);

    printf("Reefcloud firmware 1.0\n");

    printf("Chip size:%d\n", sdk_flashchip.chip_size);

    printf("wifi_get_opmode %d\n", sdk_wifi_get_opmode());
    //printf("wifi_get_opmode_default %d\n", sdk_wifi_get_opmode_default());

    sdk_wifi_station_disconnect();
    sdk_wifi_set_opmode(NULL_MODE);

	reefcloud::rtos::start_task([](void* parameter)
	{
        const int LED_GPIO = 16;

        gpio_enable(LED_GPIO, GPIO_OUTPUT);

        while(1)
        {
            gpio_write(LED_GPIO, 1);
            reefcloud::rtos::wait(200);
            gpio_write(LED_GPIO, 0);
            reefcloud::rtos::wait(200);
        }
	}, nullptr, 128);

	printf("heap %d  %d\n", (int)sdk_system_get_free_heap_size(), (int)xPortGetFreeHeapSize());

	printf("heap %d  %d\n", (int)sdk_system_get_free_heap_size(), (int)xPortGetFreeHeapSize());

	srv.initialize();
    
    printf("srv_init\n");

    srv->start();

    ip_addr_t dns_ip;
    IP4_ADDR(&dns_ip, 192, 168, 4, 1);

    //dns_srv.initialize(".reefcloud.ru#.reefcloud#.setup", dns_ip);
    dns_srv.initialize(".rcl.my#.setup.my", dns_ip);
    dns_srv->start();
}
