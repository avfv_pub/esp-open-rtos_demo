#include "reefcloud_spiffs.h"

#include <espressif/spi_flash.h>
#include <stdbool.h>
#include <esp/uart.h>
//#include <fcntl.h>
#include "esp_spiffs_flash.h"
#include "flashchip.h"
#include "esp/rom.h"
#include "esp/spi_regs.h"

/**
 * Copy 4-byte aligned source data to unaligned destination buffer.
 *
 * @param bytes Number of byte to copy to dst.
 *
 * @see unaligned_memcpy.S
 */
extern "C"
{
    void memcpy_unaligned_dst(uint8_t *dst, volatile uint32_t *src, uint8_t bytes);
}

namespace reefcloud { namespace spiffs {

/**
 * Number of file descriptors opened at the same time
 */
#define ESP_SPIFFS_FD_NUMBER       5

#define ESP_SPIFFS_CACHE_PAGES     5

// 64 bytes read causes hang
// http://bbs.espressif.com/viewtopic.php?f=6&t=2439
#define SPI_READ_MAX_SIZE   60

/**
 * Read SPI flash up to 64 bytes.
 */
static inline void IRAM read_block(sdk_flashchip_t *chip, uint32_t addr,
        uint8_t *buf, uint32_t size)
{
    SPI(0).ADDR = (addr & 0x00FFFFFF) | (size << 24);
    SPI(0).CMD = SPI_CMD_READ;

    while (SPI(0).CMD) {};

    memcpy_unaligned_dst(buf, SPI(0).W, size);
}

/**
 * Read SPI flash data. Data region doesn't need to be page aligned.
 */
static inline uint32_t IRAM read_data(sdk_flashchip_t *flashchip, uint32_t addr,
        uint8_t *dst, uint32_t size)
{
    if (size < 1) {
        return ESP_SPIFFS_FLASH_OK;
    }

    if ((addr + size) > flashchip->chip_size) {
        return ESP_SPIFFS_FLASH_ERROR;
    }

    while (size >= SPI_READ_MAX_SIZE) {
        read_block(flashchip, addr, dst, SPI_READ_MAX_SIZE);
        dst += SPI_READ_MAX_SIZE;
        size -= SPI_READ_MAX_SIZE;
        addr += SPI_READ_MAX_SIZE;
    }

    if (size > 0) {
        read_block(flashchip, addr, dst, size);
    }

    return ESP_SPIFFS_FLASH_OK;
}

uint32_t IRAM esp_spiffs_flash_read(uint32_t dest_addr, uint8_t *buf, uint32_t size)
{
    uint32_t result = ESP_SPIFFS_FLASH_ERROR;

    if (buf) {
        vPortEnterCritical();
        Cache_Read_Disable();

        result = read_data(&sdk_flashchip, dest_addr, buf, size);

        Cache_Read_Enable(0, 0, 1);
        vPortExitCritical();
    }

    return result;
}

static s32_t esp_spiffs_read(u32_t addr, u32_t size, u8_t *dst)
{
    if (esp_spiffs_flash_read(addr, dst, size) == ESP_SPIFFS_FLASH_ERROR) {
        return SPIFFS_ERR_INTERNAL;
    }

    return SPIFFS_OK;
}

// static s32_t esp_spiffs_write(u32_t addr, u32_t size, u8_t *src)
// {
//     if (esp_spiffs_flash_write(addr, src, size) == ESP_SPIFFS_FLASH_ERROR) {
//         return SPIFFS_ERR_INTERNAL;
//     }

//     return SPIFFS_OK;
// }

// static s32_t esp_spiffs_erase(u32_t addr, u32_t size)
// {
//     uint32_t sectors = size / SPI_FLASH_SEC_SIZE;
    
//     for (uint32_t i = 0; i < sectors; i++) {
//         if (esp_spiffs_flash_erase_sector(addr + (SPI_FLASH_SEC_SIZE * i))
//                 == ESP_SPIFFS_FLASH_ERROR) {
//             return SPIFFS_ERR_INTERNAL;
//         }
//     }

//     return SPIFFS_OK;
// }


filesystem::filesystem(uint32_t addr, uint32_t size)
{
    m_config.phys_addr = addr;
    m_config.phys_size = size;
    
    m_config.phys_erase_block = SPIFFS_ESP_ERASE_SIZE;
    m_config.log_page_size = SPIFFS_LOG_PAGE_SIZE;
    m_config.log_block_size = SPIFFS_LOG_BLOCK_SIZE;
    
    // Initialize fs.cfg so the following helper functions work correctly
    memcpy(&m_fs.cfg, &m_config, sizeof(::spiffs_config));

    m_work_buf.size = 2 * SPIFFS_LOG_PAGE_SIZE;
    m_fds_buf.size = SPIFFS_buffer_bytes_for_filedescs(&m_fs, ESP_SPIFFS_FD_NUMBER);
    m_cache_buf.size= SPIFFS_buffer_bytes_for_cache(&m_fs, ESP_SPIFFS_CACHE_PAGES);
    
    m_work_buf.buf = malloc(m_work_buf.size);
    m_fds_buf.buf = malloc(m_fds_buf.size);
    m_cache_buf.buf = malloc(m_cache_buf.size);
    
    m_config.hal_read_f = esp_spiffs_read;
    m_config.hal_write_f = nullptr;
    m_config.hal_erase_f = nullptr;
    
    m_config.fh_ix_offset = 3;
    
}

filesystem::~filesystem()
{
    free(m_work_buf.buf);
    m_work_buf.buf = nullptr;

    free(m_fds_buf.buf);
    m_fds_buf.buf = nullptr;

    free(m_cache_buf.buf);
    m_cache_buf.buf = nullptr;
}

int32_t filesystem::mount()
{
    printf("SPIFFS memory, work_buf_size=%d, fds_buf_size=%d, cache_buf_size=%d\n",
            m_work_buf.size, m_fds_buf.size, m_cache_buf.size);

    int32_t err = SPIFFS_mount(&m_fs, &m_config, (uint8_t*)m_work_buf.buf,
            (uint8_t*)m_fds_buf.buf, m_fds_buf.size,
            m_cache_buf.buf, m_cache_buf.size, 0);

    if (err != SPIFFS_OK) {
        printf("Error spiffs mount: %d\n", err);
    }

    return err;
}

void filesystem::unmount()
{
    SPIFFS_unmount(&m_fs);
}

::spiffs_file filesystem::open(const char* file_name)
{
    return SPIFFS_open(&m_fs, file_name, SPIFFS_RDONLY, 0);
}

void filesystem::close(::spiffs_file fd)
{
    SPIFFS_close(&m_fs, fd);
}

int32_t filesystem::get_file_size(::spiffs_file fd)
{
    ::spiffs_stat stat;
    
    int32_t err = SPIFFS_fstat(&m_fs, fd, &stat);
    if (err != SPIFFS_OK)
    {
        return -1;
    }

    return stat.size;
}

int32_t filesystem::read(::spiffs_file fd, uint8_t* buffer, int32_t buffer_size)
{
    return SPIFFS_read(&m_fs, fd, buffer, buffer_size);
}


}}