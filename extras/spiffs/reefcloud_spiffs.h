#pragma once
#include "reefcloud_system.h"
#include "reefcloud_rtos.h"

#include "spiffs.h"

namespace reefcloud { namespace spiffs {

typedef struct
{
    void *buf;
    uint32_t size;
} fs_buf_t;

class filesystem
{
public:
	filesystem(uint32_t addr, uint32_t size);
	~filesystem();

	int32_t mount();
	void unmount();

	::spiffs_file open(const char* file_name);
	int32_t get_file_size(::spiffs_file fd);
	int32_t read(::spiffs_file fd, uint8_t* buffer, int32_t buffer_size);
	void close(::spiffs_file fd);

private:
	::spiffs m_fs;
	::spiffs_config m_config = {0};
	fs_buf_t m_work_buf = {0};
	fs_buf_t m_fds_buf = {0};
	fs_buf_t m_cache_buf = {0};	
};

}}
